; Version and API
core = 7.x
api = 2

; Get Drupal core and patch that bad boy.
; projects[drupal][type] = core
projects[drupal][version] = 7.20

; Hide the profiles under /profiles this allows the installation to start at the Language selection screen
; http://drupal.org/node/1780598#comment-6480088
projects[drupal][patch][] = http://drupal.org/files/spark-install-1780598-5.patch

; This requires a core bug fix to not show the profile selection page when only
; one profile is visible.
; http://drupal.org/node/1074108#comment-6463662
projects[drupal][patch][] = http://drupal.org/files/1074108-skip-profile-16-7.x-do-not-test.patch


